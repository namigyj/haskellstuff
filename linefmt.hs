{-# LANGUAGE LambdaCase #-}

module Main where

-- future command line args
-- cmd FILE
-- -c | --column     which column should we newline
-- -s | --stdin      read from stdin instead
-- -o | --ouput      define an output file
-- -? | --?????      characters to cut-through
-- -b | --before     add newline before character
-- -a | --after      add newline after character
-- -r | --replace    replace character with newline

-- todo : replace every String by Text
-- todo : handle words longer than the max-column length

main :: IO ()
main = run getContents (theFunc [' '] Replace 80) putStr

run :: IO String -> ([String] -> String) -> (String -> IO ()) -> IO ()
run src tr out = (tr . lines) <$> src >>= out

theFunc :: [Char] -> When -> Int -> [String] -> String
theFunc chars when maxlen strs = fst $ foldl f ("", 1) strs
  where
    f (acc, _) "" = (acc++"\n\n", 1)
    f acc str     = go str "" acc
      where
        go "" word (acc', l) = (acc'++word, l)
        go (x:xs) word (acc', l) | l == maxlen     = go xs w (acc', length w)
                                 | any (==x) chars = go xs [x] (acc'++word, l+1)
                                 | otherwise       = go xs (word++[x]) (acc', l+1)
          where
            w = case when of
              Before -> '\n':word
              After -> (\(c:cs) -> c:'\n':cs) word
              Replace -> (\case {[] -> "\n"; (_:cs) -> '\n':cs}) word

data When = Before | After | Replace
