module Errors where

data Status a b = Error [a] | Ok b
                deriving (Eq, Show, Ord)

instance Semigroup (Status a b) where
    Error xs <> Errors ys = Error (as <> bs)
    Error xs <> Ok y      = Error xs
    Ok x <> Error ys      = Error ys
    Ok x <> Ok y          = Ok y

instance Functor (Status a b) where
    fmap f (Error x) = Error x
    fmap f (Ok x) = Ok (f x)
