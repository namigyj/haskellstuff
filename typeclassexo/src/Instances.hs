{-# LANGUAGE TypeFamilies #-}
module Instances where

import Data.Data (Constr(..), DataType(..), Fixity(..), mkDataType, mkConstr)
import Data.String (IsString(..))
import Data.Semigroup (Semigroup(..))

import Control.DeepSeq

import Test.QuickCheck.Arbitrary

infixr 5 :>
data List a = Nil | a :> (List a)

instance NFData a => NFData (List a) where
  rnf = rnf1

instance NFData1 List where
  liftRnf r = iter
    where
      iter Nil = ()
      iter (x:>xs) = r x `seq` iter xs

instance Show a => Show (List a) where
  show ls = "[" ++ iter ls
    where
      iter Nil = "]"
      iter (x:>Nil) = show x ++ "]"
      iter (x:>xs) = show x ++ "," ++ iter xs

instance Eq a => Eq (List a) where
  (/=) as bs =
    if length' as /= length' bs
    then True
    else any' (uncurry (/=)) $ zip' as bs
  (==) Nil     Nil = True
  (==) (a:>as) Nil = False
  (==) Nil (b:>bs) = False
  (==) (a:>as) (b:>bs) = if a == b
                         then as == bs
                         else False

instance Ord a => Ord (List a) where
  compare Nil Nil = EQ
  compare _   Nil = GT
  compare Nil _   = LT
  compare (a:>as) (b:>bs) =
      if compare a b == EQ
      then compare as bs
      else compare a b

instance (Enum a, Bounded a) => Enum (List a) where
  -- doesn't really makes sense I guess, but at least I have someting
  toEnum x = fromArr $ enumFromTo minBound (toEnum x)
  fromEnum xs = length' xs

class Eenum' a where
  eenumFrom :: a -> List a
  eenumFromThen :: a -> a -> List a
  eenumFromTo :: a -> a -> List a
  eenumFromThenTo :: a -> a -> a -> List a

instance Eenum' Int where
  eenumFrom s = eenumFromTo s maxBound
  eenumFromThen s t = eenumFromThenTo s t bnd
    where
      bnd | s < t     = maxBound
          | otherwise = minBound

  eenumFromTo s e = eft s e next stop
      where
        (next, stop) | s <= e    = (succ, (>))
                     | otherwise = (pred, (<))

  eenumFromThenTo s t e = eft s e next stop
    where
      next = \x -> x + (t - s)
      stop | s <= t = (>)
           | otherwise = (<)

instance Read a => Read (List a) where
  -- errorWithoutStackTrace "Prelude.Enum.().succ: bad argument"
  readsPrec = undefined

-- instance Data a => Data (List a) where
--   toConstr Nil = nilCtor
--   toConstr (_:>_) = consCtor

instance (a ~ Char) => IsString (List a) where
  fromString = foldr (:>) Nil

instance Monoid (List a) where
  mempty = Nil
  mappend = (++>)
  mconcat = foldr (++>)  Nil -- the acc is traversed every fucking time

instance Functor List where
  fmap f Nil    = Nil
  fmap f (x:>xs) = f x :> fmap f xs


instance Applicative List where
  pure = (:>Nil)
  fs <*> xs = concatMap' (\f -> f <$> xs) fs
  xs *> ys  = concatMap' (\_ -> fmap id ys) xs

instance Semigroup (List a) where
  (<>) = (++>)

instance Monad List where
  xs >>= f = concat $ f <$> xs
    where
      concat Nil = Nil
      concat (x:>xs) = x ++> (concat xs)
  return = pure
  xs >> ys = concatMap' (\_ -> ys) xs

instance Arbitrary a => Arbitrary (List a) where
  -- let's do nice property checking
  arbitrary = arbitrary >>= return . fromArr
  shrink = fmap fromArr . shrink . toArr

instance Foldable List where
  foldr = foldr'

-- TODO :
  -- (Const)
  -- Data
  -- Read
  -- Traversable ?
  -- Foldable ?
  -- BiFunctor, BiFoldable, Bitraversable

fromArr :: [a] -> List a
fromArr ls = foldr (:>) Nil ls
toArr :: List a -> [a]
toArr ls = foldr' (:) [] ls

length' :: List a -> Int
length' = iter 0
  where
    iter cnt Nil = cnt
    iter cnt (x:>xs) = iter (cnt+1) xs

zip' :: List a -> List b -> List (a,b)
zip' _ Nil = Nil
zip' Nil _ = Nil
zip' (a:>as) (b:>bs) = (a,b):>(zip' as bs)

any' :: (a -> Bool) -> List a -> Bool
any' p Nil = False
any' p (x:>xs) = if p x
                 then True
                 else any' p xs

take' :: Int -> List a -> List a
take' 0 _ = Nil
take' _ Nil = Nil
take' cnt (x:>xs) = x :> take' (cnt-1) xs

infixr 5 ++>
(++>) :: List a -> List a -> List a
(++>) Nil bs = bs
(++>) (a:>as) bs = a:>(as ++> bs)

eft :: Ord a => a -> a -> (a -> a) -> (a -> a -> Bool) -> List a
eft s e next stop = if s `stop` e
                    then Nil
                    else s:>eft (next s) e next stop

benchMconcat :: [List a] -> List a
benchMconcat = foldr (++>) Nil

benchMconcat' :: [List a] -> List a
benchMconcat' [] = Nil
benchMconcat' (x:xs) = go x
  where
    go Nil     = benchMconcat' xs
    go (x:>xs) = x:>go xs

nilCtor :: Constr
nilCtor = mkConstr myListDT "Nil" [] Prefix
consCtor :: Constr
consCtor = mkConstr myListDT "(:>)" [] Infix

myListDT :: DataType
myListDT = mkDataType "Main.List" [nilCtor, consCtor]

foldl' :: (b -> a -> b) -> b -> List a -> b
foldl' _ acc Nil = acc
foldl' f acc (x:>xs) = foldl' f (f acc x) xs

foldr' :: (a -> b -> b) -> b -> List a -> b
foldr' f acc Nil = acc
foldr' f acc (x:>xs) = x `f` foldr' f acc xs

concatMap' :: (a -> List b) -> List a -> List b
concatMap' f xs = foldr' (++>) Nil $ fmap f xs
