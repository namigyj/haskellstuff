{-# LANGUAGE TypeFamilies #-}

module Main where

import qualified Fns as F
import Instances
import Criterion.Main
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Function

-- write real tests, lmao
main :: IO ()
main = do
  F.tests
  --tests

benchmarks = defaultMain [
      bgroup "mconcat" [ bench "foldr" $ nf benchMconcat bigls
                       , bench "rec"   $ nf benchMconcat' bigls
                       ]
--        bgroup "eq"  [ bench "ho smol"  $ nf (/= a) b
--                     , bench "ho funcs" $ nf (/= x) y
--                     , bench "rec smol" $ nf (== a) b
--                     , bench "rec"      $ nf (== x) y
--                     ]
--      , bgroup "zip" [ bench "uncurry rec"  $ nf (uncurry ziprec)  (x, y)
--                     , bench "partial rec"  $ nf (ziprec x)  y
--                     ]
      ]
      where
        y = fromArr [1..10000] :: List Int
        bigls = foldr (:) [] $ take 1000000 $ repeat y

tests =
  let
    foo = [(+2),(*2)]
    bar = [1,2,3]
    baz = [4,5,6]
    fooL = fromArr foo
    barL = fromArr bar
    bazL = fromArr baz
    applyLs = (\ls -> fmap apply (ls :: List (Fun Int Int)))
  in
  hspec $ do
    describe "helpers" $ do
      it "fromArr [1..3]" $
        fromArr [1..3] == (1:>2:>3:>Nil) `shouldBe` True
      it "fromArr empty" $ do
        fromArr [] == (Nil::List Int) `shouldBe` True
      it "toArr empty" $ do
        toArr Nil == ([]::[Int]) `shouldBe` True
      it "toArr <[1..3]>" $ do
        toArr (1:>2:>3:>Nil) == [1..3] `shouldBe` True

    describe "Monoid Laws" $ do
      it "Associativity" $
        property (\(a,b,c) ->
                    (a ++> b) ++> c == (a ++> (b ++> c) :: List Int))
      it "Identity" $
          property $ (\a ->
                        (a ++> mempty) == a && (mempty ++> a) == (a::List Int))

    describe "Functor Laws" $ do
      it "Identity" $
        property (\a ->
                    fmap id a == (a:: List Int))
      it "Composition" $
        property (\(a,f',g') ->
                    let
                      f = apply (f' :: Fun Int Int)
                      g = apply g' -- type inference is gr8
                    in
                      fmap (f . g) a == (fmap f . fmap g) (a::List Int))
        -- TODO : generate the functions f and g too

    describe "Appllicative Laws" $ do
      it "Identity" $
        property (\a ->
                    (pure id <*> a) == (a::List Int))
      it "Composition" $ do
        property (\(a,fls1,fls2) ->
                    let fs = applyLs fls1
                        gs = applyLs fls2
                    in
                      (pure (.) <*> gs <*> fs <*> a) == (gs <*> (fs <*> a)))
        -- TODO : make a Gen (List (Int -> Int))
      it "Homomorphism" $
        property (\(a,f') ->
                    let
                      f = apply (f' :: Fun Int Int)
                    in
                      (pure f  <*> pure a) == (pure (f a) :: List Int))
      it "Interchange" $
        property (\(a, fls) ->
                    let fs = applyLs fls
                    in
                      (fs <*> pure a) == (pure($ a) <*> fs))

    describe "Applicative List versus []" $ do
      it "[f1,f2] <*> [n1,n2,n3]" $ do
        show (fooL <*> barL) == show (foo <*> bar) `shouldBe` True
      it "[n1,n2,n3] *> [m1,m2,m3]" $ do
        show (barL *> bazL)  == show (bar *> baz) `shouldBe` True
      it "[n1,n2,n3] <* [m1,m2,m3]" $ do
        show (barL <* bazL)  == show (bar <* baz) `shouldBe` True
